/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2009-2015 (C) Martin Karel
 */
package org.madsonic.booter.agent;

import org.madsonic.booter.deployer.DeploymentStatus;

/**
 * Callback interface implemented by GUI classes that wants to be notified when
 * the state of the Madsonic deployment changes.
 *
 * @author Sindre Mehus, Martin Karel
 */
public interface MadsonicListener {

    /**
     * Invoked when new information about the Madsonic deployment is available.
     *
     * @param deploymentStatus The new deployment status, or <code>null</code> if an
     *                       error occurred while retrieving the status.
     */
    void notifyDeploymentStatus(DeploymentStatus deploymentStatus);

    /**
     * Invoked when new information about the Madsonic Windows service is available.
     *
     * @param serviceStatus The new service status, or <code>null</code> if an
     *                       error occurred while retrieving the status.
     */
    void notifyServiceStatus(String serviceStatus);
}
