/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2009-2015 (C) Martin Karel
 */
package org.madsonic.booter.deployer;

import java.util.Date;
import java.io.Serializable;

/**
 * @author Sindre Mehus, Martin Karel
 */
public class DeploymentStatus implements Serializable {

	
	private final Date startTime;
    private final String url;
    private final String httpsUrl;
    private final int memoryUsed;
    private final String errorMessage;

    public DeploymentStatus(Date startTime, String url, String httpsUrl, int memoryUsed, String errorMessage) {
        this.startTime = startTime;
        this.url = url;
        this.httpsUrl = httpsUrl;
        this.memoryUsed = memoryUsed;
        this.errorMessage = errorMessage;
    }

    public String getURL() {
        return url;
    }

    public String getHttpsUrl() {
        return httpsUrl;
    }

    public Date getStartTime() {
        return startTime;
    }

    public int getMemoryUsed() {
        return memoryUsed;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
