/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2009-2015 (C) Martin Karel
 */
package org.madsonic.booter.deployer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.BindException;
import java.util.Date;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

// import org.eclipse.jetty.io.EndPoint;
// import org.eclipse.jetty.server.Request;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.server.ssl.SslSocketConnector;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.webapp.WebAppContext;
import org.madsonic.booter.util.OperatingSystem;
import org.madsonic.booter.util.WinRegistry;

/**
 * Responsible for deploying the Madsonic web app in
 * the embedded Jetty container.
 * 
 * The following system properties may be used to customize the behaviour:
 * <ul>
 * <li><code>madsonic.contextPath</code> - The context path at which Madsonic is deployed.  Default "/".</li>
 * <li><code>madsonic.port</code> - The port Madsonic will listen to.  Default 4040.</li>
 * <li><code>madsonic.httpsPort</code> - The port Madsonic will listen to for HTTPS.  Default 0, which disables HTTPS.</li>
 * <li><code>madsonic.war</code> - Madsonic WAR file, or exploded directory.  Default "madsonic.war".</li>
 * <li><code>madsonic.createLinkFile</code> - If set to "true", a Madsonic.url file is created in the working directory.</li>
 * <li><code>madsonic.ssl.keystore</code> - Path to an alternate SSL keystore.</li>
 * <li><code>madsonic.ssl.password</code> - Password of the alternate SSL keystore.</li>
 * </ul>
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MadsonicDeployer implements MadsonicDeployerService {

    public static final String DEFAULT_HOST = "0.0.0.0";
    
    public static final int DEFAULT_PORT = 4040;
    public static final int DEFAULT_HTTPS_PORT = 0;
    
    public static final int DEFAULT_MEMORY_INIT = 256;
    public static final int DEFAULT_MEMORY_LIMIT = 512;
    
    public static final String DEFAULT_CONTEXT_PATH = "/";
    
    public static final String DEFAULT_WAR = "madsonic.war";
    public static final String UPDATE_WAR = "update.war";
    
    private static final int MAX_IDLE_TIME_MILLIS = 7 * 24 * 60 * 60 * 1000; // One week.
    private static final int HEADER_BUFFER_SIZE = 64 * 1024;
    
    private static final boolean GZIP_COMPRESSION = true;

    // Madsonic home directory.
    private static final File MADSONIC_HOME_WINDOWS = new File("c:/madsonic");
    private static final File MADSONIC_HOME_OTHER = new File("/var/madsonic");
    private static final File MADSONIC_HOME_MAC = new File("/Library/Application Support/Madsonic");

    private Throwable exception;
    private File madsonicHome;
    private final Date startTime;

    public MadsonicDeployer() {

        // Enable shutdown hook for Ehcache.
        System.setProperty("net.sf.ehcache.enableShutdownHook", "true");
        startTime = new Date();
        
        createLinkFile();
        checkAutoUpdate();
        deployWebApp();
    }

    private void checkAutoUpdate() {
    	
        if ("true".equals(System.getProperty("madsonic.update"))) {
        	
	        File update = new File(getMadsonicHome()+ "/update/" + UPDATE_WAR);
	        if (!update.exists()) {
	        	update = new File(getMadsonicHome()+ "/update/" + DEFAULT_WAR);
	        }
	        InputStream in = null;
	        try {
	        	int currentBuild = Integer.valueOf(getMadsonicBuildNumber());
	            if (update.isFile()) {
	                @SuppressWarnings("resource")
					JarFile jar = new JarFile(update);
	                ZipEntry entry = jar.getEntry("WEB-INF\\classes\\build_number.txt");
	                if (entry == null) {
	                    entry = jar.getEntry("WEB-INF/classes/build_number.txt");
	                }
	                in = jar.getInputStream(entry);
	            } else {
	                in = new FileInputStream(update.getPath() + "/WEB-INF/classes/build_number.txt");
	            }
	            int updateBuild = Integer.valueOf(IOUtils.toString(in));
	            if (updateBuild > currentBuild) {
	                System.err.println("Current Build: " + currentBuild);
	                FileUtils.copyFile(update, new File(getWar()));
	                System.err.println("Updated to Build: " + updateBuild);
	            } 
	        	} catch (FileNotFoundException x) {
	            System.err.println("No newer update found.");
	            } catch (Exception x) {
	            System.err.println("Failed update from WAR: " + update);
	            x.printStackTrace();

	        } finally {
	            IOUtils.closeQuietly(in);
	        }
        }
    }
    
    private void createLinkFile() {
        if ("true".equals(System.getProperty("madsonic.createLinkFile"))) {
            Writer writer = null;
            try {
                writer = new FileWriter("madsonic.url");
                writer.append("[InternetShortcut]");
                writer.append(System.getProperty("line.separator"));
                writer.append("URL=").append(getUrl());
                writer.flush();
            } catch (Throwable x) {
                System.err.println("Failed to create madsonic.url.");
                x.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException x) {
                        // Ignored
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
	private void deployWebApp() {
        try {
            Server server = new Server();
			
            SelectChannelConnector connector = new SelectChannelConnector();
			
//            SelectChannelConnector connector = new SelectChannelConnector() {
//                public void customize(EndPoint endpoint, Request request) throws IOException {
//                    super.customize(endpoint, request);
//                    if ("https".equals(request.getHeader("X-Forwarded-Proto"))) {
//                        request.setScheme("https");
//                    }
//                }
//            };
            connector.setMaxIdleTime(MAX_IDLE_TIME_MILLIS);
            connector.setRequestHeaderSize(HEADER_BUFFER_SIZE);
            connector.setResponseHeaderSize(HEADER_BUFFER_SIZE);
            connector.setHost(getHost());
            connector.setPort(getPort());
            if (isHttpsEnabled()) {
                connector.setConfidentialPort(getHttpsPort());
            }
            server.addConnector(connector);

            if (isHttpsEnabled()) {
                SslSocketConnector sslConnector = new SslSocketConnector();
                SslContextFactory sslContextFactory = sslConnector.getSslContextFactory();
                sslContextFactory = sslConnector.getSslContextFactory();
                sslContextFactory.setExcludeCipherSuites(
                        new String[] {
                            "SSL_RSA_WITH_DES_CBC_SHA",
                            "SSL_DHE_RSA_WITH_DES_CBC_SHA",
                            "SSL_DHE_DSS_WITH_DES_CBC_SHA",
                            "SSL_RSA_EXPORT_WITH_RC4_40_MD5",
                            "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA",
                            "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA",
                            "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"
                        });
                sslContextFactory.addExcludeProtocols(new String[]{"SSLv3","SSLv2Hello"});
//              sslContextFactory.setProtocol("TLSv1.1");
                
                sslConnector.setMaxIdleTime(MAX_IDLE_TIME_MILLIS);
                sslConnector.setRequestHeaderSize(HEADER_BUFFER_SIZE);
                sslConnector.setResponseHeaderSize(HEADER_BUFFER_SIZE);
                sslConnector.setHost(getHost());
                sslConnector.setPort(getHttpsPort());

                sslConnector.setKeystore(System.getProperty("madsonic.ssl.keystore", getClass().getResource("/madsonic.keystore").toExternalForm()));
                sslConnector.setPassword(System.getProperty("madsonic.ssl.password", "madsonic"));

                server.addConnector(sslConnector);
            }

            WebAppContext context = new WebAppContext();
            context.setTempDirectory(getJettyDirectory());
            context.setContextPath(getContextPath());
            context.setWar(getWar());

            // Allow to turn off for Synology class path problems
            if (isGzipEnabled()) {
                System.err.println("Using GZIP-compression for REST");            	
            	 context.setOverrideDescriptor( MadsonicDeployer.class.getResource("/web-jetty.xml").toString() );
            }
            	 
            if (isHttpsEnabled()) {

                // Allow non-https for streaming and cover art (for Chromecast, UPnP etc)
                ((ConstraintSecurityHandler) context.getSecurityHandler()).setConstraintMappings(new ConstraintMapping[]{
                        createConstraintMapping("/stream", Constraint.DC_NONE),
                        createConstraintMapping("/coverArt.view", Constraint.DC_NONE),
                        createConstraintMapping("/ws/*", Constraint.DC_NONE),
                        createConstraintMapping("/sonos/*", Constraint.DC_NONE),
                        createConstraintMapping("/", Constraint.DC_CONFIDENTIAL)
                });
            } 

            server.setHandler(context);
            server.start();

            System.err.println("Madsonic running on: " + getUrl());
            if (isHttpsEnabled()) {
            System.err.println("                and: " + getHttpsUrl());
            }

        } catch (Throwable x) {
            x.printStackTrace();
            exception = x;
        }
    }

    private ConstraintMapping createConstraintMapping(String pathSpec, int dataConstraint) {
        ConstraintMapping constraintMapping = new ConstraintMapping();
        Constraint constraint = new Constraint();
        constraint.setDataConstraint(dataConstraint);
        constraintMapping.setPathSpec(pathSpec);
        constraintMapping.setConstraint(constraint);
        return constraintMapping;
    }

    private File getJettyDirectory() {
        File dir = new File(getMadsonicHome(), "jetty");
        String buildNumber = getMadsonicBuildNumber();
        if (buildNumber != null) {
            dir = new File(dir, buildNumber);
        }
        System.err.println("Extracting webapp to " + dir);

        if (!dir.exists() && !dir.mkdirs()) {
            System.err.println("Failed to create directory " + dir);
        }

        return dir;
    }

    private String getMadsonicBuildNumber() {
        File war = new File(getWar());
        InputStream in = null;
        try {
            if (war.isFile()) {
                @SuppressWarnings("resource")
				JarFile jar = new JarFile(war);
                ZipEntry entry = jar.getEntry("WEB-INF\\classes\\build_number.txt");
                if (entry == null) {
                    entry = jar.getEntry("WEB-INF/classes/build_number.txt");
                }
                in = jar.getInputStream(entry);
            } else {
                in = new FileInputStream(war.getPath() + "/WEB-INF/classes/build_number.txt");
            }
            return IOUtils.toString(in);

        } catch (Exception x) {
            System.err.println("Failed to resolve build number from WAR: " + war);
            x.printStackTrace();
            return null;
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    private String getContextPath() {
        return System.getProperty("madsonic.contextPath", DEFAULT_CONTEXT_PATH);
    }
   
    private boolean isGzipEnabled() {
    	boolean gzip;
    	String property = System.getProperty("madsonic.gzip", "").trim().toLowerCase();
    	switch (property) {
	    	case "true": gzip = true; break;
	    	case "false": gzip = false; break;
	    	default: gzip = GZIP_COMPRESSION; break;
    	}
    	return gzip;
    }    
    
    private String getWar() {
        String war = System.getProperty("madsonic.war");
        if (war == null) {
            war = DEFAULT_WAR;
            // Tweak Mac Default
            if (OperatingSystem.userOS == OperatingSystem.OSX) {
                war = "/Applications/Madsonic.app/Contents/Resources/Java/madsonic.war";
            }
        }
        

        File file = new File(war);
        if (file.exists()) {
            System.err.println("Using WAR file: " + file.getAbsolutePath());
        } else {
            System.err.println("Error: WAR file not found: " + file.getAbsolutePath());
        }

        return war;
    }

    private String getHost() {
        return System.getProperty("madsonic.host", DEFAULT_HOST);
    }

    private int getPort() {
        int port = DEFAULT_PORT;

        String portString = System.getProperty("madsonic.port");
        if (portString != null) {
            port = Integer.parseInt(portString);
        }

        // Also set it so that the webapp can read it.
        System.setProperty("madsonic.port", String.valueOf(port));

        return port;
    }

    private int getHttpsPort() {
        int port = DEFAULT_HTTPS_PORT;

        String portString = System.getProperty("madsonic.httpsPort");
        if (portString != null) {
            port = Integer.parseInt(portString);
        }

        // Also set it so that the webapp can read it.
        System.setProperty("madsonic.httpsPort", String.valueOf(port));

        return port;
    }

    private boolean isHttpsEnabled() {
        return getHttpsPort() > 0;
    }

    public String getErrorMessage() {
        if (exception == null) {
            return null;
        }
        if (exception instanceof BindException) {
            return "Address already in use. Please change port number.";
        }

        return exception.toString();
    }

    public int getMemoryUsed() {
        long freeBytes = Runtime.getRuntime().freeMemory();
        long totalBytes = Runtime.getRuntime().totalMemory();
        long usedBytes = totalBytes - freeBytes;
        return (int) Math.round(usedBytes / 1024.0 / 1024.0);
    }

    private String getUrl() {
        String host = DEFAULT_HOST.equals(getHost()) ? "localhost" : getHost();
        StringBuilder url = new StringBuilder("http://").append(host);
        if (getPort() != 80) {
            url.append(":").append(getPort());
        }
        url.append(getContextPath());
        return url.toString();
    }

    private String getHttpsUrl() {
        if (!isHttpsEnabled()) {
            return null;
        }

        String host = DEFAULT_HOST.equals(getHost()) ? "localhost" : getHost();
        StringBuilder url = new StringBuilder("https://").append(host);
        if (getHttpsPort() != 443) {
            url.append(":").append(getHttpsPort());
        }
        url.append(getContextPath());
        return url.toString();
    }

    /**
     * Returns the Madsonic home directory.
     *
     * @return The Madsonic home directory, if it exists.
     * @throws RuntimeException If directory doesn't exist.
     */
    private File getMadsonicHome() {

        if (madsonicHome != null) {
            return madsonicHome;
        }

        File home = null;

        String overrideHome = System.getProperty("madsonic.home");
        if (overrideHome != null) {
            home = new File(overrideHome);
		    System.out.println("used forced Install_Dir = " + home);            
        } else {
        	
        	OperatingSystem os = OperatingSystem.userOS;
        	switch (os) {
	            case WINDOWS:    home = MADSONIC_HOME_WINDOWS; break;
	            case OSX:        home = MADSONIC_HOME_MAC; break;
	            case LINUX:      home = MADSONIC_HOME_OTHER; break;
	            case UNKNOWN:    home = MADSONIC_HOME_OTHER; break;
	            default: break;
        	}

        	switch (os) {
            case WINDOWS:
            	String registryValue;
    			try {
    				registryValue = WinRegistry.readString (WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Madsonic", "Install_Dir");
    				if (registryValue != null) {
    	    		    System.out.println("used Madsonic Install_Dir = " + registryValue);
    					home = new File(registryValue);
    				}
    			} catch (Exception e) {
        		    System.out.println("use fallback Install_Dir = " + home);
    			}    
            break;
            default: break;
        	}
        }

        // Attempt to create home directory if it doesn't exist.
        if (!home.exists() || !home.isDirectory()) {
            boolean success = home.mkdirs();
            if (success) {
                madsonicHome = home;
            } else {
                String message = "The directory " + home + " does not exist. Please create it and make it writable. " +
                        "(You can override the directory location by specifying -Dmadsonic.home=... when " +
                        "starting the servlet container.)";
                System.err.println("ERROR: " + message);
            }
        } else {
            madsonicHome = home;
        }

        return home;
    }

    public DeploymentStatus getDeploymentInfo() {
        return new DeploymentStatus(startTime, getUrl(), getHttpsUrl(), getMemoryUsed(), getErrorMessage());
    }
}
