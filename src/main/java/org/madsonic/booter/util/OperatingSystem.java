/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2009-2015 (C) Martin Karel
 */
package org.madsonic.booter.util;

import java.util.Properties;

public enum OperatingSystem {
  WINDOWS,
  OSX,
  LINUX,
  UNKNOWN;

  private static OperatingSystem initOS() {
    String osName = System.getProperty("os.name");
    if(osName.equals("Mac OS X"))
      return OSX;
    if(osName.startsWith("Windows "))
      return WINDOWS;
    if(osName.startsWith("Linux"))
      return LINUX;
    return UNKNOWN;
  }

  public static final OperatingSystem userOS = initOS();

  /**
   * @return user-displayable operating system version
   */
  public static String osVersion() {
    Properties p = System.getProperties();
    String osName = p.getProperty("os.name");
    String osVersion = p.getProperty("os.version");

    if((osVersion != null) && (osVersion.length() != 0))
      osName += " " + osVersion;

    switch(userOS) {
    case OSX:
        if(osVersion.startsWith("10.2"))
            osName += " Jaguar";
          else if(osVersion.startsWith("10.3"))
            osName += " Panther";
          else if(osVersion.startsWith("10.4"))
            osName += " Tiger";
          else if(osVersion.startsWith("10.5"))
            osName += " Leopard";
          else if(osVersion.startsWith("10.6"))
            osName += " Snow Leopard";
          else if(osVersion.startsWith("10.7"))
              osName += " Lion";
          else if(osVersion.startsWith("10.8"))
              osName += " Mountain Lion";
          else if(osVersion.startsWith("10.9"))
              osName += " Mavericks";
          else if(osVersion.startsWith("10.10"))
              osName += " Yosemite";
          break;
    case WINDOWS:
      osName += " " + p.getProperty("sun.os.patch.level");
      break;
    }

    osName += " (" + p.getProperty("os.arch") + ")";
    return osName;
  }
  
  public static boolean isWindows() {
	  if (userOS == WINDOWS) {
		  return true;
	  }
	  return false;
  }

  public static String javaVersion() {
    return "Java " + System.getProperties().getProperty("java.version");
  }
}
